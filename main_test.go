package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type SumArrayByKInput struct {
	K       int
	Numbers []int
	Expect  int
}

func TestSumArrayByK(t *testing.T) {
	testCase := []SumArrayByKInput{
		{K: 3, Numbers: []int{1, 4, -1, 2, 3}, Expect: 5},
		{K: 2, Numbers: []int{1, 4, -1, 2, 3}, Expect: 5},
		{K: 0, Numbers: []int{1, 4, -1, 2, 3}, Expect: 0},
		{K: 2, Numbers: []int{}, Expect: 0},
		{K: 99, Numbers: []int{1, 4, -1, 2, 3}, Expect: 0},
		{K: 4, Numbers: []int{-1, -2, -3, -4, -5}, Expect: -10},
	}
	for _, tc := range testCase {
		result := SumArrayByK(tc.Numbers, tc.K)
		assert.Equal(t, tc.Expect, result)

	}

}
