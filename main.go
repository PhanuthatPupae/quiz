package main

import (
	"fmt"
)

func main() {
	// result := SumArrayByK([]int{1, 4, -1, 2, 3}, 3)
	// fmt.Println("====>", result)
	result := SumArrayByK([]int{1, 4, -1, 2, 3}, 3)
	fmt.Println("====>", result)
}

/*
A: สร้าง function รับ array ของตัวเลขจำนวนเต็ม และจำนวนเต็ม k แล้วให้หาผลรวมมากที่สุดของจำนวนที่ติดกัน k ตัว

โดย interface ของ function จะเป็น
fn(inputs: Array<Number>, k: Number) -> Number

Example input and output:

#1 example:
fn([1, 4, -1, 2, 3], 3)  expected return 5 (เกิดจาก 4 -1 +2)

#2 example:
fn([1, 4, -1, 2, 3], 2)  expected return 5 (เกิดจาก 2 + 3)
*/

func SumArrayByK(numbers []int, k int) int {
	result := 0
	if len(numbers) <= 0 {
		return 0
	}
	if k <= 0 {
		return 0
	}

	if k > len(numbers) {
		return 0
	}

	sumList := []int{}

	// round := math.Ceil(float64(len(numbers)) / float64(k))
	for i := 0; i < len(numbers); i++ {
		sum := 0
		count := 1
		for j := i; j < len(numbers); j++ {

			if count > k {
				break
			}
			fmt.Println("hello", j, i)

			count++

			if j < len(numbers) {
				sum += numbers[j]
			}

		}
		if count < k {
			break
		}
		fmt.Println(sumList)
		sumList = append(sumList, sum)
	}
	if len(sumList) > 0 {
		result = sumList[0]
		for _, s := range sumList {
			if s > result {
				result = s
			}
		}
	}

	return result
}

/*

B: สมมุติมีสถานการณ์คุณกำลังเดินไปข้างหน้าเรื่อยๆ แล้วระหว่างทางมีสมบัติให้เก็บ จนไปถึงเส้นชัย
ให้หาว่าระหว่างทางคุณสามารถเก็บสมบัติได้มากที่สุดเท่าใด

โดยมีเงื่อนไขว่า
ถ้าคุณเก็บสมบัติชิ้นใด สมบัติจะพาคุณวาป ไปข้างเสมอ และคุณไม่สามารถเดินย้อนกลับไปเก็บสมบัติชิ้นก่อนได้ และสมบัติแต่ละชิ้นมีค่าต่างกันไปในแต่ละชิ้น และในแต่ละจุดจะมีสมบัติเพียงชิ้นเดียวเท่านั้น

หมายเหตุ: วาปคือการเคลื่อนย้ายคุณไป ณ จุดที่สมบัติกำหนดทันที (Teleport)

หน้าที่ของคุณคือให้หามูลค่ามากที่สุดที่คุณสามารถเก็บได้เมื่อถึงเส้นชัย

ตัวอย่าง
คุณเริ่มต้นที่ 0 เมตร และเส้นชัยอยู่ที่ 10 เมตร
และสมบัติมีทั้งหมด 3 ชิ้น

ชิ้นที่ 1 อยู่ที่ระยะ 0 เมตร มูลค่า 100 แต่ต้องวาปไปที่ที่ระยะ 10 เมตร (เส้นชัย)
ชั้นที่ 2 อยู่ที่ระยะ 2 เมตร มูลค่า 2 และต้องวาปไปที่ระยะ 3 เมตร
ชิ้นที่ 3 อยู่ที่ระยะ 8 เมตร มูลค่า 99 และต้องวาปไปที่ระยะ 9 เมตร


ภาพประกอบ

Figure 1

จากข้อมูลข้างต้นถ้าคุณเก็บสมบัติชิ้นแรกที่ระยะ 0m คุณจะถูกวาปไปที่เส้นชัยเลย และทำให้คุณมีสมบัติมูลค่าทั้งหมด 100

แต่ถ้าคุณเลือกไม่เก็บสมบัติชิ้นแรก และเดินต่อไปเรื่อยๆ เก็บชิ้นที่ 2 คุณจะวาปไปที่ 3m และเดินต่อไปเรื่อยๆ เก็บชิ้นที่ 3 และวาปไปที่ 9m และเดินไปเรื่อยๆจนถึงเส้นชัย จะทำให้คุณมีสมบัติมูลค่าทั้งหมด 101

คำตอบของตัวอย่างนี้ก็คือ 101

ให้คุณสร้าง function ในการหาคำตอบจากโจทย์ดังกล่าวโดยมี function หน้าตาคร่าวๆดังต่อไปนี้

type Treasure = {
  position: int <เมตร>
  reward: int
  wrapToPosition: int <เมตร>
}

fn(goalAt: int <เมตร>, treasures: Array<Treasure>): int

*หน้าตา function ไม่จำเป็นต้องเหมือน 100% สามารถปรับเปลี่ยนไปตามที่ถนัดได้ แต่ input หลักๆจะมี 2 ตัวคือ

1. goalAt ระบุว่า goal อยู่ที่เมตรที่เท่าใด
2. treasures ระบุสมบัติทั้งหมดที่มีในแต่ละจุด โดย input ที่ให้ถือว่าเรียงตาม position จากน้อยไปมากให้แล้ว

จากตัวอย่าง input จะเป็น

fn(
  goalAt: 10,
  treasures: [
    { position: 0, reward: 100, wrapToPosition: 10 },
    { position: 2, reward: 2, wrapToPosition: 3 },
    { position: 8, reward: 99, wrapToPosition: 9 },
  ]
)
และค่าสูงสุดของแต่ละตัวแปรคือ

0 < goalAt <= 100,000
For simple case: 0 <= length of treasures <= 20
For bonus: 0 <= length of treasures < 100,000

1. treasure[i].position < treasure[j].position สำหรับทุกๆที่ i < j // กล่าวคือ array input ถูกเรียงมาให้แล้ว และจะไม่ซ้ำกันในแต่ละจุด
2. treasure[i].position < goalAt สำหรับ i ใดๆ // กล่าวคือสมบัติสามารถมีอยู่ได้ทุกจุดยกเว้นจุดเส้นชัย
3. 0 < Treasure[i].reward < 1000 สำหรับ i ใดๆ
4. treasure[i].warpToPosition > treasure[i].position สำหรับ i ใดๆ // กล่าวคือถ้าเก็บสมบัติ จะวาปไปข้างหน้าเสมอ
5. treasure[i].warpToPosition <= goalAt สำหรับ i ใดๆ



*/

type Treasure struct {
	Position       int
	Reward         int
	WrapToPosition int
}

func FindMaxTotalReward(goalAt int, treasureList []Treasure) int {
	result := 0
	for idx1, t1 := range treasureList {
		for idx2, t2 := range treasureList {
			if idx2 == 0 {
				continue
			}
			fmt.Println(t2)
		}
		fmt.Println(idx1, t1)
	}
	return result

}
